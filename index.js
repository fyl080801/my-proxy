const express = require("express")
const { createProxyMiddleware } = require("http-proxy-middleware")

const app = express()
const port = 3000

app.get("/hello", (req, res) => {
  res.send("Hello World!")
})

app.use(
  "/api",
  createProxyMiddleware({
    target: process.env.TARGET || "http://example.com",
    changeOrigin: true,
    pathRewrite: {
      "^/api": ""
    }
  })
)

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`)
})
